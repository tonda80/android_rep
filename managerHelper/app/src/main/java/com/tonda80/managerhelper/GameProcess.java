package com.tonda80.managerhelper;

import android.content.SharedPreferences;
import android.util.Pair;

import java.util.Vector;


class Actor {
    public String name;
    public int sum;

    public Actor(String name_, int sum_) {
        name = name_;
        sum = sum_;
    }
}


class GameProcess {

    private final String BankName = "_Bank";
    private final String JackpotName = "_Jackpot";
    private final String BoxName = "_Box";        // белый\черный бизнес оно же благотворительность

    boolean is_special_actor(int i) {
        return i < 3;
    }
    boolean is_special_actor(String name) {
        return name.startsWith("_");
    }

    String app_name(String name) {
        if (is_special_actor(name)) {
            return name.substring(1);
        }
        return name;
    }

    private final int BeginBankSum = 1000000;
    private final int BeginJackpotSum = 0;
    private final int BeginBoxSum = 0;
    private final int BeginPlayerSum = 2000;

    private Vector<Actor> actors;

    private SharedPreferences prefs;

    GameProcess(SharedPreferences prefs_) {
        prefs = prefs_;

        reinit_actors();
    }

    private void reinit_actors() {
        if (actors == null) {
            actors = new Vector<>();
        }
        else {
            actors.clear();
        }

        actors.add(new Actor(BankName, prefs.getInt(BankName, BeginBankSum)));
        actors.add(new Actor(JackpotName, prefs.getInt(JackpotName, BeginJackpotSum)));
        actors.add(new Actor(BoxName, prefs.getInt(BoxName, BeginBoxSum)));

        for (String s : prefs.getAll().keySet()) {
            if (!is_special_actor(s)) {
                actors.add(new Actor(s, prefs.getInt(s, BeginPlayerSum)));
            }
        }
    }

    String add_player(String name) {
        if (is_special_actor(name)) {
            return String.format("Name '%s' is wrong", name);
        }
        for (Actor a : actors) {
            if (a.name.equals(name)) {
                return String.format("Name '%s' already exists", name);
            }
        }

        Actor new_actor = new Actor(name, BeginPlayerSum);
        actors.add(new_actor);
        save(new_actor);
        return "";
    }

    void reset() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        reinit_actors();
    }

    void new_game() {
        for (Actor a : actors) {
            switch (a.name) {
                case BankName:
                    a.sum = BeginBankSum;
                    break;
                case JackpotName:
                    a.sum = BeginJackpotSum;
                    break;
                case BoxName:
                    a.sum = BeginBoxSum;
                    break;
                default:
                    a.sum = BeginPlayerSum;
            }
        }

        save_all();
    }

    void save_all() {
        SharedPreferences.Editor editor = prefs.edit();
        for (Actor a : actors) {
            editor.putInt(a.name, a.sum);
        }
        editor.apply();
    }

    private void save(Actor actor) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(actor.name, actor.sum);
        editor.apply();
    }

    int actor_quantity() {
        return actors.size();
    }

    String name_by_index(int i) {
        return app_name(actors.elementAt(i).name);
    }

    // предполагаемое количество денег в поле ввода
    String likely_amount(int src_idx, int dst_idx) {
        if (src_idx == 0)
            return "200";
        else if (src_idx == 1 || src_idx == 2) {
            return String.valueOf(sum_by_index(src_idx));
        }
        else if (dst_idx == 1)
            return "100";
        else if (dst_idx == 2)
            return "50";

        return "";
    }

    int sum_by_index(int i) {
        return actors.elementAt(i).sum;
    }

    Pair<Boolean, String> _mkp(boolean b, String s) {
        return new Pair<Boolean, String>(b, s);
    }

    Pair<Boolean, String> make_transaction(int src, int dst, String value) {
        Actor src_actor = actors.elementAt(src);
        Actor dst_actor = actors.elementAt(dst);

        int sum = 0;
        if (value.startsWith("#")) {
            int percent = 0;
            try {
                percent = Integer.parseInt(value.substring(1));
            }
            catch (Exception e) {
                return _mkp(false, value.substring(1) + "is not integer");
            }
            if (percent < 1 || percent > 99) {
                return _mkp(false,"Wrong transaction percent value: " + percent);
            }
            if (src == 0 && !is_special_actor(dst)) {
                sum = dst_actor.sum*percent/100;
            }
            else if (dst == 0 && !is_special_actor(src)) {
                sum = src_actor.sum*percent/100;
            }
            else {
                return _mkp(false,"Percent transaction not for Bank and player");
            }
        }
        else {
            try {
                sum = Integer.parseInt(value);
            }
            catch (Exception e) {
                return _mkp(false,value + "is not integer");
            }
        }

        if (sum < 1) {
            return _mkp(false,"Wrong transaction value: " + sum);
        }
        if (sum > src_actor.sum) {
            return _mkp(false, String.format("Not enough money from source. There is %d but %d is needed", src_actor.sum, sum));
        }

        src_actor.sum -= sum;
        dst_actor.sum += sum;
        save_all();

        return _mkp(true, String.valueOf(sum));
    }

}
