package com.tonda80.managerhelper;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.InputType;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;


class C {
    static final String Tag = "managerHelper";
}


class OnTouchListener implements View.OnTouchListener {
    MainActivity owner;
    float x_down, y_down;

    OnTouchListener(MainActivity owner_) {
        owner = owner_;
    }

    @Override
    public boolean onTouch(View v, MotionEvent ev) {
        //Log.d(C.Tag, ev.toString());
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x_down = ev.getX(0);
                y_down = ev.getY(0);
                return true;
            case MotionEvent.ACTION_UP:
                owner.swipe_action(x_down, y_down, ev.getX(0), ev.getY(0));
                return true;
            // UP без DOWN судя по всему не бывает
        }
        return false;
    }
}


public class MainActivity extends AppCompatActivity {

    private GameProcess gameProcess;
    private TableLayout main_layout;
    private Vector<TextView> sums;

    private TextView log_view;
    private SimpleDateFormat log_time_format;
    private ScrollView log_scroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        main_layout = findViewById(R.id.table_lt);
        main_layout.setOnTouchListener(new OnTouchListener(this));

        log_view = findViewById(R.id.log_tv);
        log_scroll = findViewById(R.id.log_scroll);
        log_time_format = new SimpleDateFormat("[HH:mm:ss]");

        sums = new Vector<>();

        gameProcess = new GameProcess(getPreferences(Context.MODE_PRIVATE));
        init_players();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        gameProcess.save_all();
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_game:
                gameProcess.new_game();
                update_sums();
                clear_log_story();
                add_log("---- Game is started! ----");
                return true;
            case R.id.action_add_player:
                create_add_player_dialog();
                return true;
            case R.id.action_reset:
                gameProcess.reset();
                init_players();
                clear_log_story();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void report_error(String what) {
        Toast.makeText(getApplicationContext(), what, Toast.LENGTH_LONG).show();
        add_log("[ERROR]: " + what);
    }

    private void create_add_player_dialog() {
        final EditText edt = new EditText(this);
        edt.setSingleLine();
        edt.requestFocus();
        edt.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("New player")
                .setMessage("Enter name").setView(edt)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String new_name = edt.getText().toString().trim();
                        String res = gameProcess.add_player(new_name);
                        if (res.isEmpty()) {
                            init_players();
                        }
                        else {
                            report_error(res);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {}
                });
        show_dialog_at_top(builder.create());
    }

    private void init_players() {
        final float FontSize = 50;

        main_layout.removeAllViews();
        sums.clear();

        for (int i = 0; i < gameProcess.actor_quantity(); ++i) {
            TableRow tableRow = new TableRow(this);

            TextView name_tv = new TextView(this);
            name_tv.setText(gameProcess.name_by_index(i));
            name_tv.setTextSize(FontSize);

            TextView sum_tv = new TextView(this);
            sum_tv.setTextSize(FontSize);

            if (gameProcess.is_special_actor(i)) {
                name_tv.setTypeface(null, Typeface.BOLD);
                sum_tv.setTypeface(null, Typeface.BOLD);
            }

            sums.add(sum_tv);

            tableRow.addView(name_tv, 0);
            tableRow.addView(sum_tv, 1);
            main_layout.addView(tableRow, i);
        }
        update_sums();
    }

    void update_sums() {
        for (int i = 0; i < sums.size(); ++i) {
            sums.elementAt(i).setText(String.valueOf(gameProcess.sum_by_index(i)));
        }
    }

    private int get_row_index(float x, float y) {
        for (int i = 0; i < main_layout.getChildCount(); ++i) {
            View v = main_layout.getChildAt(i);
            Rect rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
            if(rect.contains((int)x, (int)y)) {
                return i;
            }
        }
        return -1;
    }

    public void swipe_action(float x_src, float y_src, float x_dst, float y_dst) {
        int src = get_row_index(x_src, y_src);
        int dst = get_row_index(x_dst, y_dst);
        //Log.d(C.Tag, String.format("[swipe_action] %d => %d", src, dst));

        //  || (gameProcess.is_special_actor(src) && gameProcess.is_special_actor(dst))
        if (dst == -1 || src == -1 || dst == src) {
            return;
        }
        create_transaction_dialog(src, dst);
    }

    private void create_transaction_dialog(final int src, final int dst) {
        String src_name = gameProcess.name_by_index(src);
        String dst_name = gameProcess.name_by_index(dst);
        Log.d(C.Tag, String.format("[swipe_action] %s(%d) => %s(%d)", src_name, src, dst_name, dst));

        final String transact_desc = String.format("%s => %s", src_name, dst_name);

        final EditText edt = new EditText(this);
        edt.setSingleLine();
        edt.setInputType(InputType.TYPE_CLASS_PHONE);  // TYPE_CLASS_NUMBER TYPE_CLASS_TEXT
        edt.requestFocus();     // открытие клавиатуры

        String likely_amount = gameProcess.likely_amount(src, dst);
        if (!likely_amount.isEmpty()) {
            edt.setText(likely_amount, TextView.BufferType.NORMAL);
            edt.selectAll();
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(transact_desc)
                .setMessage("Enter sum (# percents)").setView(edt)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        do_transfer(src, dst, edt, transact_desc);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {}
                })
        ;
        final AlertDialog dlg = builder.create();
        show_dialog_at_top(dlg);

        // повесим действие на клавиатурный ввод
        edt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    do_transfer(src, dst, edt, transact_desc);
                    dlg.dismiss();
                }

                return false;
            }
        });

    }

    private void show_dialog_at_top(AlertDialog dlg) {
        Window window = dlg.getWindow();

        //WindowManager.LayoutParams wlp = new WindowManager.LayoutParams();
        //wlp.copyFrom(window.getAttributes());
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        //wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //wlp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(wlp);
        // будет желание, разобраться с этим бредом
        // копирование и изменение "напрямую" приводит к разным результатам
        // так же может влиять setAttributes после show

        dlg.show();
    }

    private void do_transfer(int src, int dst, EditText edt, String transact_desc) {
        String value = edt.getText().toString().trim();
        Pair<Boolean, String> res  = gameProcess.make_transaction(src, dst, value);
        if (res.first) {
            update_sums();
            add_log(String.format("%s\t\t%s", transact_desc, res.second));
        }
        else {
            report_error(res.second);
        }
    }

    private void clear_log_story() {
        log_view.setText("");
    }

    private void add_log(String msg) {
        String ts = log_time_format.format(new Date());
        log_view.append(String.format("%s  %s\n", ts, msg));

        log_scroll.fullScroll(ScrollView.FOCUS_DOWN);
    }
}
