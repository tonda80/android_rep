#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring JNICALL
Java_com_bee_ant_hello_1ndk_MainActivity_stringFromJNI(
		JNIEnv *env,
		jobject /* this */) {
	std::string hello = "Hello from C++!\n";
	return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_bee_ant_hello_1ndk_MainActivity_mult10(JNIEnv *env, jclass /*  */, jint n) {
	return n*10;
}
